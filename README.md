# Fortran Telegram Bot Library

## Dependencies
- [json-fortran](https://github.com/jacobwilliams/json-fortran)
- [zofu](https://github.com/acroucher/zofu)

## Env vars:
- `JSONFORTRAN_PATH` (default: `/usr/local/jsonfortran-gnu-6.10.0/lib`)
	- the path where the json-fortran library is installed (`json_file_module.mod` and `libjsonfortran.so`)
- `LD_LIBRARY_PATH`
	- Add the json-fortran library path to your `LD_LIBRARY_PATH`. **This is needed before every start of the bot, so better add it to your bashrc.**
- `ZOFU_LIB_PATH` (default: `/usr/local/lib`)
	- zofu library path (where `libzofu.so` is)
- `ZOFU_INC_PATH` (default: `/usr/local/include`)
	- zofu include path (where `zofu.mod` is)

## Building
1. Install the json-fortran library
3. `cd src && make`
4. `./src/main`

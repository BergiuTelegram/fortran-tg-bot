module test_helper
    use zofu
    use helper
    implicit none
    public test_helper_all
    private

contains
    subroutine test_helper_all(test)
        implicit none
        class(unit_test_type) :: test
        call test_fulltrim_char_to_char(test)
        call test_fulltrim_char_to_alloc(test)
        call test_fulltrim_alloc_to_char(test)
        call test_fulltrim_alloc_to_alloc(test)
    end subroutine

    subroutine test_fulltrim_char_to_char(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        character(len=5) :: b
        b = fulltrim("  Hallo  ")
        call test%assert(b, "Hallo", "test_fulltrim_char_to_char: same string")
        call test%assert(len(b), 5, "test_fulltrim_char_to_char: same length")
    end subroutine

    subroutine test_fulltrim_char_to_alloc(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        character(:), allocatable :: b
        b = fulltrim("  Hallo  ")
        call test%assert(b, "Hallo", "test_fulltrim_char_to_alloc: same string")
        call test%assert(len(b), 5, "test_fulltrim_char_to_alloc: same length")
    end subroutine

    subroutine test_fulltrim_alloc_to_alloc(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        character(:), allocatable :: a
        character(:), allocatable :: b
        a = "  Hallo  "
        b = fulltrim(a)
        call test%assert(b, "Hallo", "test_fulltrim_alloc_to_alloc: same string")
        call test%assert(len(b), 5, "test_fulltrim_alloc_to_alloc: same length")
    end subroutine

    subroutine test_fulltrim_alloc_to_char(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        character(:), allocatable :: a
        character(len=5) :: b
        a = "  Hallo  "
        b = fulltrim(a)
        call test%assert(b, "Hallo", "test_fulltrim_alloc_to_char: same string")
        call test%assert(len(b), 5, "test_fulltrim_alloc_to_char: same length")
    end subroutine

end module

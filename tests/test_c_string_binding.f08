module test_c_string_binding
    use zofu
    use iso_c_binding
    use c_string_binding
    implicit none
    public test_c_string_binding_all
    private

contains
    subroutine test_c_string_binding_all(test)
        implicit none
        class(unit_test_type) :: test
        call test_get_c_test_string(test)
        call test_c_f_string_ptr(test)
        call test_c_f_string_alloc(test)
    end subroutine

    subroutine test_get_c_test_string(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(c_string), pointer                 :: u
        allocate(u)
        call get_c_test_string(u)
        call test%assert(int(u%length), 5, "get_c_test_string")
    end subroutine

    subroutine test_c_f_string_ptr(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(c_string), pointer                 :: u
        character(:),   pointer                 :: str
        allocate(u)
        call get_c_test_string(u)
        call c_f_string_ptr(u, str)
        call test%assert(str, "Hallo", "c_f_string_ptr")
    end subroutine

    subroutine test_c_f_string_alloc(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(c_string), pointer                 :: u
        character(:),   allocatable             :: str
        allocate(u)
        call get_c_test_string(u)
        call c_f_string_alloc(u, str)
        call test%assert(str, "Hallo", "c_f_string_alloc")
    end subroutine

end module

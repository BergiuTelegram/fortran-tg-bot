module test_url_builder
    use zofu
    use url_builder
    implicit none
    public test_url_builder_all
    private

contains
    subroutine test_url_builder_all(test)
        implicit none
        class(unit_test_type) :: test
        call test_add_param_character_chars(test)
        call test_add_param_character_ints(test)
        call test_add_param_character_both(test)
        call test_add_param_character_char_space(test)
        call test_add_param_character_int_space(test)
        call test_add_param_no_val(test)
    end subroutine

    subroutine test_add_param_character_chars(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(url_param) :: a
        call a%add_param("key", "val")
        call a%add_param("key2", "val2")
        call test%assert(a%param, "key=val&key2=val2", "test_add_param_character_chars: adding char values")
    end subroutine

    subroutine test_add_param_character_ints(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(url_param) :: a
        call a%add_param("key", 5)
        call a%add_param("key2", 7)
        call test%assert(a%param, "key=5&key2=7", "test_add_param_character_ints: adding int values")
    end subroutine

    subroutine test_add_param_character_both(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(url_param) :: a
        call a%add_param("key", "val")
        call a%add_param("key2", 7)
        call a%add_param("key3", "val2")
        call a%add_param("key4", 5)
        call test%assert(a%param, "key=val&key2=7&key3=val2&key4=5", "test_add_param_character_both: adding char and int values")
    end subroutine

    subroutine test_add_param_character_char_space(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(url_param) :: a
        call a%add_param("  key  ", "  val  ")
        call test%assert(a%param, "key=val", "test_add_param_character_char_space: adding char vals with spaces")
    end subroutine

    subroutine test_add_param_character_int_space(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(url_param) :: a
        call a%add_param("  key  ", 5)
        call test%assert(a%param, "key=5", "test_add_param_character_char_space: adding int vals with spaces")
    end subroutine

    subroutine test_add_param_no_val(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(url_param) :: a
        call a%add_param("  key  ")
        call test%assert(a%param, "key=", "test_add_param_no_val: adding key without val")
    end subroutine

end module

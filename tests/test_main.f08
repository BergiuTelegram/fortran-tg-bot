program test_main
    use zofu
    use test_datastructs
    use test_c_string_binding
    use test_url_builder
    use test_helper
    implicit none
    type(unit_test_type) :: test
    call test%init()
    call test%assert(.true.)
    call test_datastructs_all(test)
    call test_c_string_binding_all(test)
    call test_url_builder_all(test)
    call test_helper_all(test)
    call test%summary()
end program

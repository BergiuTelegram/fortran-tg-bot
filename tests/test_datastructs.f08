module test_datastructs
    use iso_c_binding
    use datastructs
    use zofu
    implicit none
    public test_datastructs_all
    private

contains
    subroutine test_datastructs_all(test)
        implicit none
        class(unit_test_type) :: test
        call test_create_user_min_params(test)
        call test_create_user_max_params(test)
        call test_create_example_user(test)
        call test_create_chat_min_params(test)
        call test_create_chat_max_params(test)
        call test_create_example_chat(test)
        call test_create_message_min_params(test)
        call test_create_message_some_params(test)
        call test_create_message_reply(test)
    end subroutine

!!!!!!!!!!!!!!!!!!!
! CREATE EXAMPLES !
!!!!!!!!!!!!!!!!!!!

    ! depends on create_user is working
    function create_example_user() result(res)
        type(tguser)                            :: res
        integer(kind=C_INT32_T),    parameter       :: id = 299184608
        logical,                parameter       :: is_bot = .true.
        character(len=*),       parameter       :: first_name = "spielwiese"
        character(len=*),       parameter       :: last_name = "spielen"
        character(len=*),       parameter       :: username = "spielwiese_bot"
        res = create_user(id, is_bot, first_name, last_name, username)
    end function

    ! depends on create_chat is working
    function create_example_chat() result(res)
        type(tgchat)                            :: res
        integer(kind=C_INT64_T),    parameter       :: id = -1001250337914_C_INT64_T
        character(len=*),       parameter       :: chattype = "group"
        character(len=*),       parameter       :: title = "Anime & IT Talk"
        character(len=*),       parameter       :: username = "@anime_ger"
        character(len=*),       parameter       :: first_name = "Anime"
        character(len=*),       parameter       :: last_name = "IT Talk"
        res = create_chat(id, chattype, title, username, first_name, last_name)
    end function

    ! depends on create_message_is_working
    function create_example_message() result(res)
        type(tgchat)                            :: res
    end function

!!!!!!!!!!!!!!!!!!!!!!!!!!!
! TEST CREATE_DATASTRUCTS !
!!!!!!!!!!!!!!!!!!!!!!!!!!!

    !!!!!!!!!!!!!!!
    ! CREATE USER !
    !!!!!!!!!!!!!!!
    subroutine test_create_user_min_params(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tguser)                            :: u
        integer(kind=C_INT32_T),    parameter       :: id = 299184608
        logical,                parameter       :: is_bot = .true.
        character(len=*),       parameter       :: first_name = "spielwiese"
        u = create_user(id, is_bot, first_name)
        call test%assert(u%id, id, "create_user_min_params: user%id")
        call test%assert(u%is_bot, is_bot, "create_user_min_params: user%is_bot")
        call test%assert(u%first_name, first_name, "create_user_min_params: user%first_name")
        call test%assert(u%last_name_exists, .false., "create_user_min_params: user%last_name_exists")
        call test%assert(u%username_exists, .false., "create_user_min_params: user%username_exists")
    end subroutine

    subroutine test_create_user_max_params(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tguser)                            :: u
        integer(kind=C_INT32_T),    parameter       :: id = 299184608
        logical,                parameter       :: is_bot = .true.
        character(len=*),       parameter       :: first_name = "spielwiese"
        character(len=*),       parameter       :: last_name = "spielen"
        character(len=*),       parameter       :: username = "spielwiese_bot"
        u = create_user(id, is_bot, first_name, last_name, username)
        call test%assert(u%id, id, "create_user: user%id")
        call test%assert(u%is_bot, is_bot, "create_user: user%is_bot")
        call test%assert(u%first_name, first_name, "create_user: user%first_name")
        call test%assert(u%last_name, last_name, "create_user: user%last_name")
        call test%assert(u%last_name_exists, .true., "create_user_max_params: user%last_name_exists")
        call test%assert(u%username, username, "create_user: user%username")
        call test%assert(u%username_exists, .true., "create_user_max_params: user%username_exists")
    end subroutine

    subroutine test_create_example_user(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tguser)                            :: u
        u = create_example_user()
    end subroutine

    !!!!!!!!!!!!!!!
    ! CREATE CHAT !
    !!!!!!!!!!!!!!!
    subroutine test_create_chat_min_params(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tgchat)                            :: c
        integer(kind=C_INT64_T),    parameter       :: id = -1001250337914_C_INT64_T
        character(len=*),       parameter       :: chattype = "group"
        c = create_chat(id, chattype)
        call test%assert(c%id, id, "create_chat: chat%id")
        call test%assert(c%chattype, chattype, "create_chat: chat%chattype")
        call test%assert(c%title_exists, .false., "create_chat_min_params: chat%title_exists")
        call test%assert(c%username_exists, .false., "create_chat_min_params: chat%username_exists")
        call test%assert(c%first_name_exists, .false., "create_chat_min_params: chat%first_name_exists")
        call test%assert(c%last_name_exists, .false., "create_chat_min_params: chat%last_name_exists")
    end subroutine

    subroutine test_create_chat_max_params(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tgchat)                            :: c
        integer(kind=C_INT64_T),    parameter       :: id = -1001250337914_C_INT64_T
        character(len=*),       parameter       :: chattype = "group"
        character(len=*),       parameter       :: title = "Anime & IT Talk"
        character(len=*),       parameter       :: username = "@anime_ger"
        character(len=*),       parameter       :: first_name = "Anime"
        character(len=*),       parameter       :: last_name = "IT Talk"
        c = create_chat(id, chattype, title, username, first_name, last_name)
        call test%assert(c%id, id, "create_chat_max_params: chat%id")
        call test%assert(c%chattype, chattype, "create_chat_max_params: chat%chattype")
        call test%assert(c%title, title, "create_chat_max_params: chat%title")
        call test%assert(c%username, username, "create_chat_max_params: chat%username")
        call test%assert(c%username_exists, .true., "create_chat_max_params: chat%username_exists")
        call test%assert(c%first_name, first_name, "create_chat_max_params: chat%first_name")
        call test%assert(c%first_name_exists, .true., "create_chat_max_params: chat%first_name_exists")
        call test%assert(c%last_name, last_name, "create_chat_max_params: chat%last_name")
        call test%assert(c%last_name_exists, .true., "create_chat_max_params: chat%last_name_exists")
    end subroutine

    subroutine test_create_example_chat(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tgchat)                            :: c
        c = create_example_chat()
    end subroutine

    !!!!!!!!!!!!!!!!!!
    ! CREATE MESSAGE !
    !!!!!!!!!!!!!!!!!!
    subroutine test_create_message_min_params(test)
        implicit none
        class(unit_test_type),  intent(inout)       :: test
        type(tgmessage)                             :: m
        integer(kind=C_INT32_T),            parameter   :: message_id = 58614
        type(tguser), pointer                       :: from
        integer(kind=C_INT32_T),            parameter   :: date = 1539549795
        type(tgchat), pointer                       :: chat
        allocate(from)
        allocate(chat)
        from = create_example_user()
        chat = create_example_chat()
        m = create_message(message_id, from, date, chat)
        call test%assert(m%message_id, message_id, "create_message_min_params: message%message_id")
        call test%assert(m%from%id, from%id, "create_message_min_params: message%from%id")
        call test%assert(m%date, date, "create_message_min_params: message%date")
        call test%assert(m%chat%id, chat%id, "create_message_min_params: message%chat%id")
        deallocate(chat)
        deallocate(from)
        deallocate(m%chat)
        deallocate(m%from)
    end subroutine

    subroutine test_create_message_some_params(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tgchat)                            :: u
    end subroutine

    subroutine test_create_message_reply(test)
        implicit none
        class(unit_test_type),  intent(inout)   :: test
        type(tgchat)                            :: u
    end subroutine
end module

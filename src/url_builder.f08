module url_builder
    use helper
    implicit none
    private

    type, public :: url_param
        character(:), allocatable :: param
    contains
        procedure       :: add_param_character
        procedure       :: add_param_integer
        generic, public :: add_param => add_param_character, &
            add_param_integer
    end type

contains
    subroutine add_param_character(param, key, val)
        class(url_param),       intent(inout)   :: param
        character(*),           intent(in)      :: key
        character(*), optional, intent(in)      :: val
        if(len(param%param) == 0) then
            param%param = fulltrim(key) // "="
            if(present(val)) param%param = param%param // fulltrim(val)
        else
            param%param = param%param // "&" // fulltrim(key) // "="
            if(present(val)) param%param = param%param // fulltrim(val)
        end if
    end subroutine

    subroutine add_param_integer(param, key, val)
        class(url_param),   intent(inout)   :: param
        character(*),       intent(in)      :: key
        integer,            intent(in)      :: val
        character(len=20)                   :: val_cast_orig
        character(:),       allocatable     :: val_cast
        write (val_cast_orig, fmt="(i0)") val
        val_cast = fulltrim(val_cast_orig)
        if(len(param%param) == 0) then
            param%param = fulltrim(key) // "=" // fulltrim(val_cast)
        else
            param%param = param%param // "&" // fulltrim(key) // "=" // fulltrim(val_cast)
        end if
    end subroutine
end module

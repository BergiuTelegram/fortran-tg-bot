module helper
    implicit none
    public fulltrim
    private

contains
    function fulltrim(str) result(o)
        implicit none
        character(*), intent(in)    :: str
        character(:), allocatable   :: o
        o = trim(adjustl(str))
    end function

end module

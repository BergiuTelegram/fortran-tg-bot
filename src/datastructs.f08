module datastructs
    use iso_c_binding
    use json_module
    implicit none
    public create_user, create_user_json, user_to_json, user_to_string
    public create_chat
    public create_message, create_message_json, message_to_json, message_to_string
    private

    type, public :: tguser
        integer(kind=C_INT32_T)             :: id
        logical                         :: is_bot
        character(len=:),   allocatable :: first_name
        character(len=:),   allocatable :: last_name    ! optional
        logical                         :: last_name_exists
        character(len=:),   allocatable :: username     ! optional
        logical                         :: username_exists
    end type tguser

    type, public :: tgchat
        integer(kind=C_INT64_T)             :: id
        character(len=:),   allocatable :: chattype     ! can be either "private", "group", "supergroup", "channel"
        character(len=:),   allocatable :: title        ! optional
        logical                         :: title_exists
        character(len=:),   allocatable :: username     ! optional
        logical                         :: username_exists
        character(len=:),   allocatable :: first_name   ! optional
        logical                         :: first_name_exists
        character(len=:),   allocatable :: last_name    ! optional
        logical                         :: last_name_exists
    end type tgchat

    type, public :: tgmessage
        integer(kind=C_INT32_T)             :: message_id
        type(tguser),       pointer     :: from
        integer(kind=C_INT32_T)             :: date
        type(tgchat),       pointer     :: chat                     ! conversation the message belongs to
        type(tguser),       pointer     :: forward_from             ! optional
        logical                         :: forward_from_exists
        type(tgchat),       pointer     :: forward_from_chat        ! optional
        logical                         :: forward_from_chat_exists
        integer(kind=C_INT32_T)             :: forward_from_message_id  ! optional
        logical                         :: forward_from_message_id_exists
        type(tgmessage),    pointer     :: reply_to_message         ! optional
        logical                         :: reply_to_message_exists
        character(len=:),   allocatable :: text                     ! optional
        logical                         :: text_exists
    end type tgmessage

contains
    pure function create_user(id, is_bot, first_name, last_name, username) &
            result(u)
        implicit none
        type(tguser)                                    :: u
        integer(kind=C_INT32_T),    intent(in)              :: id
        logical,                intent(in)              :: is_bot
        character(len=*),       intent(in)              :: first_name
        character(len=*),       intent(in), optional    :: last_name
        character(len=*),       intent(in), optional    :: username
        u%id          = id
        u%is_bot      = is_bot
        u%first_name  = first_name
        if(present(last_name)) then
            u%last_name         = last_name
            u%last_name_exists  = .true.
        else
            u%last_name_exists  = .false.
        end if
        if(present(username)) then
            u%username          = username
            u%username_exists   = .true.
        else
            u%username_exists   = .false.
        end if
    end function create_user

    pure function create_chat(id, chattype, title, username, first_name, last_name) &
            result(c)
        implicit none
        type(tgchat)                                    :: c
        integer(kind=C_INT64_T),    intent(in)              :: id
        character(len=*),       intent(in)              :: chattype
        character(len=*),       intent(in), optional    :: title
        character(len=*),       intent(in), optional    :: username
        character(len=*),       intent(in), optional    :: first_name
        character(len=*),       intent(in), optional    :: last_name
        c%id = id
        c%chattype = chattype
        if(present(title)) then
            c%title = title
            c%title_exists = .true.
        else
            c%title_exists = .false.
        end if
        if(present(username)) then
            c%username = username
            c%username_exists = .true.
        else
            c%username_exists = .false.
        end if
        if(present(first_name)) then
            c%first_name = first_name
            c%first_name_exists = .true.
        else
            c%first_name_exists = .false.
        end if
        if(present(last_name)) then
            c%last_name = last_name
            c%last_name_exists = .true.
        else
            c%last_name_exists = .false.
        end if
    end function create_chat

    function create_message(message_id, from, date, chat, forward_from, &
            forward_from_chat, forward_from_message_id, reply_to_message, text) &
            result(m)
        implicit none
        type(tgmessage)                                 :: m
        integer(kind=C_INT32_T),                intent(in)  :: message_id
        type(tguser),                       intent(in)  :: from
        integer(kind=C_INT32_T),                intent(in)  :: date
        type(tgchat),                       intent(in)  :: chat
        type(tguser),           optional,   intent(in)  :: forward_from
        type(tgchat),           optional,   intent(in)  :: forward_from_chat
        integer(kind=C_INT32_T),    optional,   intent(in)  :: forward_from_message_id
        type(tgmessage),        optional,   intent(in)  :: reply_to_message
        character(len=*),       optional,   intent(in)  :: text
        allocate(m%from)
        allocate(m%chat)
        allocate(m%forward_from)
        allocate(m%forward_from_chat)
        allocate(m%reply_to_message)
        m%message_id = message_id
        m%from = from
        m%date = date
        m%chat = chat
        if(present(forward_from)) then
            m%forward_from = forward_from
            m%forward_from_exists = .true.
        else
            m%forward_from_exists = .false.
        end if
        if(present(forward_from_chat)) then
            m%forward_from_chat = forward_from_chat
            m%forward_from_chat_exists = .true.
        else
            m%forward_from_chat_exists = .false.
        end if
        if(present(forward_from_message_id)) then
            m%forward_from_message_id = forward_from_message_id
            m%forward_from_message_id_exists = .true.
        else
            m%forward_from_message_id_exists = .false.
        end if
        if(present(reply_to_message)) then
            m%reply_to_message = reply_to_message
            m%reply_to_message_exists = .true.
        else
            m%reply_to_message_exists = .false.
        end if
        if(present(text)) then
            m%text = text
            m%text_exists = .true.
        else
            m%text_exists = .false.
        end if
    end function create_message

    function create_user_json(u, json, prefix) result(error)
        implicit none
        logical                                     :: error
        type(tguser),       intent(out)             :: u
        type(json_file),    intent(inout)           :: json
        character(len=*),   intent(in), optional    :: prefix
        character(len=:),   allocatable             :: prefix_intern
        logical                                     :: found
        integer(kind=C_INT32_T)                         :: id
        logical                                     :: is_bot
        character(len=:),   allocatable             :: first_name
        character(len=:),   allocatable             :: last_name    ! optional
        character(len=:),   allocatable             :: username     ! optional
        error = .false.
        if(present(prefix)) then
            prefix_intern = prefix
        else
            prefix_intern = ""
        end if
        call json%get(prefix_intern//'id', id, found)
        if ( found ) then
            u%id = id
        else
            error = .true.
        end if
        call json%get(prefix_intern//'is_bot', is_bot, found)
        if ( found ) then
            u%is_bot = is_bot
        else
            error = .true.
        end if
        call json%get(prefix_intern//'first_name', first_name, found)
        if ( found ) then
            u%first_name = first_name
        else
            error = .true.
        end if
        call json%get(prefix_intern//'last_name', last_name, found)
        if ( found ) then
            u%last_name           = last_name
            u%last_name_exists    = .true.
        else
            u%last_name_exists    = .false.
        end if
        call json%get(prefix_intern//'username', username, found)
        if ( found ) then
            u%username        = username
            u%username_exists = .true.
        else
            u%username_exists = .false.
        end if
    end function create_user_json

    function create_chat_json(c, json, prefix) result(error)
        implicit none
        logical                                     :: error
        type(tgchat),       intent(out)             :: c
        type(json_file),    intent(inout)           :: json
        character(len=*),   intent(in), optional    :: prefix
        character(len=:),   allocatable             :: prefix_intern
        logical                                     :: found
        logical                                     :: error_tmp
        integer(kind=C_INT64_T)                         :: id
        character(len=:),   allocatable             :: id_string
        character(len=:),   allocatable             :: chattype
        character(len=:),   allocatable             :: title        ! optional
        character(len=:),   allocatable             :: username     ! optional
        character(len=:),   allocatable             :: first_name   ! optional
        character(len=:),   allocatable             :: last_name    ! optional
        error = .false.
        if(present(prefix)) then
            prefix_intern = prefix
        else
            prefix_intern = ""
        end if
        call json%get(prefix_intern//'id', id_string, found)
        if(found) then
            read(id_string, *) id
            error = .false.
        else
            error = .true.
        end if
        call json%get(prefix_intern//'chattype', chattype, found)
        if(found) then
            c%chattype = chattype
        else
            error = .true.
        end if
        call json%get(prefix_intern//'title', title, found)
        if(found) then
            c%title = title
            c%title_exists = .true.
        else
            c%title_exists = .false.
        end if
        call json%get(prefix_intern//'username', username, found)
        if(found) then
            c%username = username
            c%username_exists = .true.
        else
            c%username_exists = .false.
        end if
        call json%get(prefix_intern//'first_name', first_name, found)
        if(found) then
            c%first_name = first_name
            c%first_name_exists = .true.
        else
            c%first_name_exists = .false.
        end if
        call json%get(prefix_intern//'last_name', last_name, found)
        if(found) then
            c%last_name = last_name
            c%last_name_exists = .true.
        else
            c%last_name_exists = .false.
        end if

    end function create_chat_json

    function create_message_json(m, json, prefix) result(error)
        implicit none
        logical                                     :: error
        type(tgmessage),    intent(out)             :: m
        type(json_file),    intent(inout)           :: json
        character(len=*),   intent(in), optional    :: prefix
        character(len=:),   allocatable             :: prefix_intern
        logical                                     :: found
        logical                                     :: error_tmp
        integer(kind=C_INT32_T)                 :: message_id
        character(len=:),   allocatable     :: from_string
        type(tguser),         pointer       :: from
        integer(kind=C_INT32_T)                 :: date
        character(len=:),   allocatable     :: chat_string
        type(tgchat),         pointer       :: chat
        character(len=:),   allocatable     :: forward_from_string      ! optional
        type(tguser),         pointer       :: forward_from             ! optional
        character(len=:),   allocatable     :: forward_from_chat_string ! optional
        type(tgchat),         pointer       :: forward_from_chat        ! optional
        integer(kind=C_INT32_T)                 :: forward_from_message_id  ! optional
        character(len=:),   allocatable     :: reply_to_message_string  ! optional
        type(tgmessage),      pointer       :: reply_to_message         ! optional
        character(len=:),   allocatable     :: text                     ! optional
        if(present(prefix)) then
            prefix_intern = prefix
        else
            prefix_intern = ""
        end if
        call json%get(prefix_intern//'message_id', message_id, found)
        if ( found ) then
            m%message_id = message_id
        else
            error = .true.
        end if
        call json%get(prefix_intern//'from', from_string, found)
        if ( found ) then
            error_tmp = create_user_json(from, json, prefix_intern//"from")
            if (error_tmp) error = .true.
            m%from = from
        else
            error = .true.
        end if
        call json%get(prefix_intern//'date', date, found)
        if ( found ) then
            m%date = date
        else
            error = .true.
        end if
        ! call json%get(prefix_intern//'chat', chat_string, found)
        ! if ( found ) then
        !     error_tmp = create_chat_json(chat, json, prefix_intern//"from")
        !     if (error_tmp) error = error_tmp
        !     m%chat = chat
        ! else
        !     error = .true.
        ! end if
        call json%get(prefix_intern//'forward_from', forward_from_string, found)
        if ( found ) then
            error_tmp = create_user_json(forward_from, json, prefix_intern//"forwarded_from")
            if (error_tmp) error = .true.
            m%forward_from = forward_from
            m%forward_from_exists = .true.
        else
            m%forward_from_exists = .false.
        end if
        call json%get(prefix_intern//'forward_from_chat', forward_from_chat_string, found)
        ! if ( found ) then
        !     error_tmp = create_chat_json(forward_from_chat, json, prefix_intern//"forwarded_from_chat")
        !     if (error_tmp) error = .true.
        !     m%forward_from_chat = forward_from_chat
        !     m%forward_from_chat_exists = .true.
        ! else
        !     m%forward_from_chat_exists = .false.
        ! end if
        call json%get(prefix_intern//'forward_from_message_id', forward_from_message_id, found)
        if ( found ) then
            m%forward_from_message_id = forward_from_message_id
            m%forward_from_message_id_exists = .true.
        else
            m%forward_from_message_id_exists = .false.
        end if
        ! RECURSIVE
        ! call json%get(prefix_intern//'reply_to_message', reply_to_message_string, found)
        ! if ( found ) then
        !     error_tmp = create_message_json(reply_to_message, json, prefix_intern//"reply_to_message")
        !     if (error_tmp) error = .true.
        !     m%reply_to_message = reply_to_message
        !     m%reply_to_message = .true.
        ! else
        !     m%reply_to_message = .false.
        ! end if
        call json%get(prefix_intern//'text', text, found)
        if ( found ) then
            m%text = text
            m%text_exists = .true.
        else
            m%text_exists = .false.
        end if
    end function create_message_json

    function user_to_json(u) result(j)
        type(json_file)             :: j
        type(tguser), intent(in)    :: u
        logical                     :: found
        call j%add('id', u%id)
        call j%add('is_bot', u%is_bot)
        call j%add('first_name', u%first_name)
        if (u%last_name_exists) call j%add('last_name', u%last_name)
        if (u%username_exists) call j%add('username', u%username)
    end function user_to_json

    function message_to_json(m) result(j)
        implicit none
        type(json_file)             :: j
        type(tgmessage), intent(in) :: m
        logical                     :: found
        call j%add('message_id', m%message_id)
        ! call j%add('from', m%from)
        call j%add('date', m%date)
        ! call j%add('chat', m%chat)
        ! if (m%forward_from_exists) call j%add('forward_from', m%forward_from)
        ! if (m%forward_from_chat_exists) call j%add('forward_from_chat', m%forward_from_chat)
        if (m%forward_from_message_id_exists) call j%add('forward_from_message_id', m%forward_from_message_id)
        ! if (m%reply_to_message_exists) call j%add('reply_to_message', m%reply_to_message)
        if (m%text_exists) call j%add('text', m%text)
    end function message_to_json

    function user_to_string(u) result(s)
        character(:), allocatable :: s
        type(tguser), intent(in) :: u
        type(json_file) :: j
        j = user_to_json(u)
        call j%print_to_string(s)
        call j%destroy()
    end function

    function message_to_string(m) result(s)
        character(:), allocatable :: s
        type(tgmessage), intent(in) :: m
        type(json_file) :: j
        j = message_to_json(m)
        call j%print_to_string(s)
        call j%destroy()
    end function

end module datastructs

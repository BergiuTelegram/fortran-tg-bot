module c_string_binding
    use :: iso_c_binding
    implicit none
    public c_f_string_ptr, c_f_string_alloc
    private

    type, bind(c), public :: c_string
        type(c_ptr)         :: string ! Pointer to the first character
        integer(c_size_t)   :: length ! Length of the string
    end type

contains
    ! Cast an array of characters(len=1) to a normal character(len=:)
    subroutine get_scalar_pointer(scalar_len, scalar, ptr)
        use, intrinsic :: iso_c_binding, only: c_char
        integer, intent(in) :: scalar_len
        character(kind=c_char,len=scalar_len), intent(in), target :: scalar(1)
        character(:,kind=c_char), intent(out), pointer :: ptr
        ptr => scalar(1)
    end subroutine get_scalar_pointer

    ! Cast c string to fortran string pointer
    subroutine c_f_string_ptr(c_str, f_str_ptr)
        type(c_string),   pointer, intent(in)    :: c_str
        character(len=:), pointer, intent(inout) :: f_str_ptr
        character(len=1), pointer                :: tmp_reply(:) ! Used to cast the reply
        call c_f_pointer(c_str%string, tmp_reply, [c_str%length])
        call get_scalar_pointer(size(tmp_reply), tmp_reply, f_str_ptr)
    end subroutine

    ! Cast c string to fortran string allocatable
    subroutine c_f_string_alloc(c_str, f_str_alloc)
        type(c_string),   pointer,      intent(in)  :: c_str
        character(len=:), allocatable,  intent(out) :: f_str_alloc
        character(len=:), pointer                   :: f_str_ptr
        call c_f_string_ptr(c_str, f_str_ptr)
        f_str_alloc = f_str_ptr
    end subroutine
end module c_string_binding

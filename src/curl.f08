module curl
    use :: iso_c_binding
    use :: c_string_binding
    implicit none
    public curl_init, curl_cleanup, get, post, curl_example
    private

    ! curl_global_init flags
    integer(c_long), public :: CURL_GLOBAL_NOTHING      = 0_c_long
    integer(c_long), public :: CURL_GLOBAL_SSL          = 1_c_long
    integer(c_long), public :: CURL_GLOBAL_WIN32        = 2_c_long
    integer(c_long), public :: CURL_GLOBAL_ALL          = 3_c_long
    integer(c_long), public :: CURL_GLOBAL_DEFAULT      = 3_c_long
    integer(c_long), public :: CURL_GLOBAL_ACK_EINTR    = 4_c_long

    ! CURLoption
    integer(c_int), public :: CURLOPT_WRITEDATA         = 10001
    integer(c_int), public :: CURLOPT_URL               = 10002
    integer(c_int), public :: CURLOPT_COPYPOSTFIELDS    = 10165
    integer(c_int), public :: CURLOPT_WRITEFUNCTION     = 20011

    interface
        ! Callback for curls write_data function.
        function write_data_callback(buffer, bsize, nmemb, userp) bind(c) result(res)
            use :: iso_c_binding
            integer(c_size_t)                       :: res
            type(c_ptr),        value,  intent(in)  :: buffer
            integer(c_size_t),  value,  intent(in)  :: bsize
            integer(c_size_t),  value,  intent(in)  :: nmemb
            type(c_ptr),        value,  intent(in)  :: userp
        end function
    end interface

    interface
        !!!!!!!!!!!!!!!
        ! CURL GLOBAL !
        !!!!!!!!!!!!!!!
        function curl_global_init(flags) &
                bind(c, name="curl_global_init") &
                result(res)
            use iso_c_binding
            implicit none
            integer(c_long), value, intent(in) :: flags
            integer(c_int)                     :: res
        end function curl_global_init

        subroutine curl_global_cleanup() &
                bind(c, name="curl_global_cleanup")
        end subroutine curl_global_cleanup

        !!!!!!!!!!!!!
        ! CURL EASY !
        !!!!!!!!!!!!!
        function curl_easy_init() &
                bind(c, name="curl_easy_init") &
                result(res)
            use iso_c_binding
            implicit none
            type(c_ptr) :: res
        end function curl_easy_init

        ! Strings must be null terminated. Use the wrapper curl_easy_setopt
        ! instead of c_curl_easy_setopt.
        function c_curl_easy_setopt_string(curl_ptr, option, param) &
                bind(c, name="curl_easy_setopt") &
                result(res)
            use iso_c_binding
            implicit none
            integer(c_int)                          :: res
            type(c_ptr),        value,  intent(in)  :: curl_ptr
            integer(c_int),     value,  intent(in)  :: option
            character(len=1),           intent(in)  :: param
        end function c_curl_easy_setopt_string

        ! Sets a callback. Use c_funloc(func) to get the c address of func.
        function c_curl_easy_setopt_cb(curl_ptr, option, callback) &
                bind(c, name="curl_easy_setopt") &
                result(res)
            use iso_c_binding
            implicit none
            integer(c_int)                          :: res
            type(c_ptr),        value,  intent(in)  :: curl_ptr
            integer(c_int),     value,  intent(in)  :: option
            type(c_funptr),     value,  intent(in)  :: callback
        end function c_curl_easy_setopt_cb

        ! Sets a pointer. Use c_loc(obj) to get the c address of obj.
        function c_curl_easy_setopt_ptr(curl_ptr, option, ptr) &
                bind(c, name="curl_easy_setopt") &
                result(res)
            use iso_c_binding
            implicit none
            integer(c_int)                      :: res
            type(c_ptr),    value,  intent(in)  :: curl_ptr
            integer(c_int), value,  intent(in)  :: option
            type(c_ptr),    value,  intent(in)  :: ptr
        end function c_curl_easy_setopt_ptr

        function curl_easy_perform(curl_ptr) &
                bind(c, name="curl_easy_perform") &
                result(res)
            use iso_c_binding
            implicit none
            integer(c_int)                      :: res
            type(c_ptr),    value,  intent(in)  :: curl_ptr
        end function curl_easy_perform

        subroutine curl_easy_cleanup(curl_ptr) &
                bind(c, name="curl_easy_cleanup")
            use iso_c_binding
            implicit none
            type(c_ptr),    value,  intent(in)  :: curl_ptr
        end subroutine curl_easy_cleanup

        subroutine curl_easy_reset(handle) &
                bind(c, name="curl_easy_reset")
            use iso_c_binding
            implicit none
            type(c_ptr),    value,  intent(in)  :: handle
        end subroutine curl_easy_reset
    end interface

contains
    ! Wrapper for c_curl_easy_setopt_string. Automatically nullterminates the string for c.
    function curl_easy_setopt_string(curl_ptr, option, param) &
            result(res)
        use iso_c_binding
        implicit none
        integer(c_int)                      :: res
        type(c_ptr),    value,  intent(in)  :: curl_ptr
        integer(c_int), value,  intent(in)  :: option
        character(len=*),       intent(in)  :: param
        res = c_curl_easy_setopt_string(curl_ptr, option, trim(param)//char(0))
    end function curl_easy_setopt_string

    ! Writes the buffer and nmemb into userp.
    function write_data(buffer, bsize, nmemb, userp) result(res)
        use iso_c_binding
        integer(c_size_t)                       :: res    ! Set to nmemb
        type(c_ptr),        value,  intent(in)  :: buffer ! The buffer that contains the c string
        integer(c_size_t),  value,  intent(in)  :: bsize  ! Always 1; ignored.
        integer(c_size_t),  value,  intent(in)  :: nmemb  ! Length of the c string
        type(c_ptr),        value,  intent(in)  :: userp  ! C Struct to write the buffer into
        type(c_string),     pointer             :: u      ! Fortran representation of userp
        call c_f_pointer(userp, u)
        u%string = buffer
        u%length = nmemb
        res = nmemb
    end function write_data

    ! Prepares a handle and sets the url and the write data callback.
    subroutine prepare_handle(handle, url, userp)
        type(c_ptr),        intent(inout)           :: handle ! Curl handler
        character(len=*),   intent(in)              :: url    ! The url to get
        type(c_string),     intent(inout),  pointer :: userp  ! Passed into curl to get the string
        procedure(write_data_callback),     pointer :: wdcb => write_data
        integer(c_int)                              :: ret
        ! Set url
        ret = curl_easy_setopt_string(handle, CURLOPT_URL, url)
        ! Set write data pointer to userp, to let it beeing passed into write_data_callback
        ret = c_curl_easy_setopt_ptr(handle, CURLOPT_WRITEDATA, c_loc(userp))
        ! Set the callback of the curl function to write_data
        ret = c_curl_easy_setopt_cb(handle, CURLOPT_WRITEFUNCTION, c_funloc(wdcb))
    end subroutine

    !!!!!!!!!!
    ! PUBLIC !
    !!!!!!!!!!
    ! Must be called before using curl.
    ! Thread hostile. Don't call it when any other thread is running.
    subroutine curl_init()
        implicit none
        integer(c_int) :: ret
        ret = curl_global_init(CURL_GLOBAL_ALL)
    end subroutine curl_init

    ! Must be called after using curl.
    ! Thread hostile. Don't call it when any other thread is running.
    subroutine curl_cleanup()
        implicit none
        call curl_global_cleanup()
    end subroutine curl_cleanup

    subroutine get(url, reply)
        implicit none
        character(len=*),   intent(in)              :: url    ! The url to get
        character(len=:),   intent(inout),  pointer :: reply  ! The string replied by curl
        type(c_ptr)                                 :: handle ! Curl handler
        type(c_string),                     pointer :: userp  ! Passed into curl to get the string
        integer(c_int)                              :: ret
        allocate(userp)
        handle = curl_easy_init()
        if (c_associated(handle, c_null_ptr)) then
            print*,"error in curl_easy_init"
            return
        end if
        ! Prepare request
        call prepare_handle(handle, url, userp)
        ! Execute the request
        ret = curl_easy_perform(handle)
        ! Cast strings
        call c_f_string_ptr(userp, reply)
    end subroutine get

    subroutine post(url, postdata, reply)
        implicit none
        character(len=*),   intent(in)              :: url      ! The url to send data to
        character(len=*),   intent(in)              :: postdata ! The data to send to the url
        character(len=:),   intent(inout),  pointer :: reply    ! The string replied by curl
        type(c_ptr)                                 :: handle   ! Curl handler
        type(c_string),                     pointer :: userp    ! Passed into curl to get the string
        integer(c_int)                              :: ret
        allocate(userp)
        handle = curl_easy_init()
        if (c_associated(handle, c_null_ptr)) then
            print*,"error in curl_easy_init"
        end if
        ! Prepare request
        call prepare_handle(handle, url, userp)
        ! Add postdata to request
        ret = curl_easy_setopt_string(handle, CURLOPT_COPYPOSTFIELDS, postdata)
        ! Execute the request
        ret = curl_easy_perform(handle)
        ! Cast strings
        call c_f_string_ptr(userp, reply)
    end subroutine

    subroutine curl_example()
        implicit none
        character(:), pointer :: reply
        ! First initialize curl. Remember that this is not thread save.
        call curl_init()
        ! Make a get request.
        call get("http://localhost/get.php?username=gelo", reply)
        print*,"ergebnis: ",reply
        ! Make a post request.
        call post("http://localhost/post.php", "username=myrtol", reply)
        print*,"ergebnis: ",reply
        ! Clean curl. This is also not thread save.
        call curl_cleanup()
    end subroutine curl_example
end module curl

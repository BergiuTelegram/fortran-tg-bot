module telegram_api
    use iso_c_binding
    use json_module
    use helper
    use datastructs
    use curl
    use url_builder
    implicit none
    public getMe, init, show_token, newbot, example
    private
    character(len=*), parameter :: api_url  = "https://api.telegram.org/bot"

    type, public :: telegram_bot
        character(len=100)          :: tg_token
        type(tguser)                  :: bot_user
    end type

contains
    !!!!!!!!!!!!!!!!!!
    ! CONFIG THE BOT !
    !!!!!!!!!!!!!!!!!!

    subroutine newbot(bot, token)
        implicit none
        type(telegram_bot), intent(out) :: bot
        character(len=*), intent(in)    :: token
        bot%tg_token = token
    end subroutine newbot

    function init(bot) result(error)
        implicit none
        type(telegram_bot), intent(inout)   :: bot
        logical                             :: error
        error = getMe(bot, bot%bot_user)
    end function init

    subroutine show_token(bot)
        implicit none
        type(telegram_bot), intent(in) :: bot
        print*,bot%tg_token
    end subroutine

    !!!!!!!!!!!
    ! HELPERS !
    !!!!!!!!!!!

    subroutine webcall(bot, reply, method, params)
        implicit none
        type(telegram_bot), intent(in)                  :: bot
        character(len=:),   intent(inout),  pointer     :: reply
        character(len=*),   intent(in)                  :: method
        character(len=*),   intent(in),     optional    :: params
        character(len=2000)                          :: url
        url = trim(api_url) // trim(bot%tg_token) // "/" // trim(method) // trim(params)
        print*,"Calling url ", trim(url)
        call get(trim(url), reply)
    end subroutine webcall

    ! Tries to find an error message in the telegram reply.
    ! If it finds an error, it returns true and prints an error message.
    function is_error(j) result(res)
        implicit none
        logical                         :: res
        type(json_file), intent(inout)  :: j
        logical                         :: ok
        logical                         :: found_ok
        integer                         :: error_code
        logical                         :: found_error_code
        character(:), allocatable       :: description
        logical                         :: found_description
        call j%get("ok", ok, found_ok)
        res = .false.
        if(.not. found_ok) then
            print*,"Error: Tg reply has no key 'ok'"
            res = .true.
        else if(.not. ok) then
            call j%get("error_code", error_code, found_error_code)
            call j%get("description", description, found_description)
            if(.not. found_error_code) then
                print*,"Error: Tg reply has no key 'error_code'"
                res = .true.
            else if(.not. found_description) then
                print*,"Error: Tg reply has no key 'description'"
                res = .true.
            else
                write(unit=*, fmt="(a,i4,a,a)") "Error: Tg replied errorcode ", &
                    error_code, ": ", description
                res = .true.
            end if
        end if
    end function is_error

    !!!!!!!!!!!!!
    ! API CALLS !
    !!!!!!!!!!!!!

    function getMe(bot, u) result(error)
        implicit none
        logical                         :: error
        type(telegram_bot), intent(in)  :: bot
        type(tguser), intent(out)         :: u
        character(len=:), pointer       :: reply
        character(len=:), allocatable   :: s
        type(json_file)                 :: j
        call webcall(bot, reply, "getMe")
        call j%load_from_string(reply)
        if(is_error(j)) then
            print*,reply
            error = .true.
        else
            error = create_user_json(u, j, "result.")
            s = user_to_string(u)
            print*,s
        end if
        call j%destroy()
    end function getMe

    function sendMessage(bot, m, chat_id, text, reply_to_message_id) result(error)
        implicit none
        type(telegram_bot),                 intent(in)  :: bot
        integer(kind=C_INT32_T),                intent(in)  :: chat_id
        character(len=*),                   intent(in)  :: text
        integer(kind=C_INT32_T),    optional,   intent(in)  :: reply_to_message_id
        type(tgmessage)                                   :: m          ! on success, the sent message is returned
        character(:),           pointer                 :: reply
        type(url_param)                                 :: params
        character(len=:), allocatable                   :: s
        type(json_file)                                 :: j
        logical                                         :: error
        call params%add_param("chat_id", chat_id)
        call params%add_param("text", text)
        if(present(reply_to_message_id)) call params%add_param("reply_to_message_id", reply_to_message_id)
        call webcall(bot, reply, "sendMessage", "?" // params%param)
        call j%load_from_string(reply)
        if(is_error(j)) then
            print*,reply
            error = .true.
        else
            call j%print_file()
            error = create_message_json(m, j, "result.")
            s = message_to_string(m)
            print*,s
        end if
        call j%destroy()
    end function sendMessage

    !!!!!!!!
    ! REST !
    !!!!!!!!

    subroutine example(token_filename)
        implicit none
        character(len=*), intent(in)    :: token_filename
        type(telegram_bot)              :: bot
        logical                         :: error
        character(len=100)              :: token
        type(tgmessage)                   :: m
        open (unit=15, file=token_filename, status='old', &
            access='sequential', form='formatted', action='read')
        read (15, fmt="(A)") token
        call newbot(bot, token)
        error = init(bot)
        error = sendMessage(bot, m, 23767443, "Hä ß🙃 llo")
    end subroutine example
end module telegram_api


JSONFORTRAN_PATH ?= "/usr/local/jsonfortran-gnu-7.0.0/lib"
ZOFU_LIB_PATH ?= "/usr/local/lib"
ZOFU_INC_PATH ?= "/usr/local/include"

MAIN_OUT = main
TEST_OUT = test

FC = gfortran
CC = gcc
MKDIR_P = mkdir -p
SDIR = src
TDIR = tests
ODIR = obj
FINC = -I$(JSONFORTRAN_PATH) -I$(ZOFU_INC_PATH)
FLIBS = -L$(JSONFORTRAN_PATH) -L$(ZOFU_LIB_PATH)
FLINK = -lcurl -ljsonfortran -lzofu -lc
FFLAGS = -fno-underscoring
# CINC = -I$(JSONFORTRAN_PATH) -I$(ZOFU_INC_PATH)
# CLIBS = -L$(JSONFORTRAN_PATH) -L$(ZOFU_LIB_PATH)
# CLINK = -lcurl -ljsonfortran -lzofu -lc
# CFLAGS = -fno-underscoring

_OBJS = helper.o c_string_binding.o curl.o url_builder.o datastructs.o telegram_api.o
_TOBJS = test_helper.o test_url_builder.o test_datastructs.o test_c_string_binding.o test_c_string_binding_helper.o

_MAIN_OBJS = $(_OBJS) main.o
MAIN_OBJS = $(patsubst %,$(ODIR)/%,$(_MAIN_OBJS))

_TEST_OBJS = $(_OBJS) $(_TOBJS) test_main.o
TEST_OBJS = $(patsubst %,$(ODIR)/%,$(_TEST_OBJS))

all: build build_tests

$(ODIR)/%.o: */%.f08
	mkdir -p $(@D)
	$(FC) -c $(FINC) -o $@ -J $(ODIR) $< $(FFLAGS)

$(ODIR)/%.o: */%.c
	mkdir -p $(@D)
	$(CC) -c $(CINC) -o $@ -J $(ODIR) $< $(CFLAGS)

$(MAIN_OUT): $(MAIN_OBJS)
	mkdir -p $(@D)
	$(FC) -o $@ $^ $(FLIBS) $(FLINK)

$(TEST_OUT): $(TEST_OBJS)
	mkdir -p $(@D)
	$(FC) -o $@ $^ $(FLIBS) $(FLINK)

build: $(MAIN_OUT)

build_tests: $(TEST_OUT)

.PHONY: all clean build build_tests

clean:
	rm -Rf $(ODIR) $(MAIN_OUT) $(TEST_OUT)

